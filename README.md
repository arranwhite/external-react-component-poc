Proof of concept that pulls a React component and other code from an external source.

Proves that we can fetch and integrate React components using a shared React instance.


```
#!shell

cd external-component
npm install
npm run build
cp external.js ../main-app/external.js

cd ../main-app
npm install
npm run start
open http://localhost:8080
```