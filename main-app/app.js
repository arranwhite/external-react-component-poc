import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class LocalComponent extends React.Component {
    render() {
        return (
            <div>
                <p>Hello from a local component</p>
                { this.props.children }
            </div>
        );
    }
}

// Put React on global scope so that external code can use it.
global.React = React;

// Create this function for external code to execute when it loads.
global.bootstrapFincalc = function(data) {
    // We would probably fire an action here so we could update our store...
    console.log('External code callback!', data);
    const { ExternalComponent, runMe } = data;
    runMe();

    // Render a local component and the external component!
    ReactDOM.render(
        <LocalComponent>
            <ExternalComponent />
        </LocalComponent>
    , document.getElementById('app'));
}
