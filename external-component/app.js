import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class ExternalComponent extends Component {
    render() {
        return <div>I am an external component</div>;
    }
}

function runMe() {
    console.log('Hello from external code!')
}

// Expecting the 'bootstrapFincalc' function to be declared in the environment...
bootstrapFincalc({
    ExternalComponent,
    runMe
});
