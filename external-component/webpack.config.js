module.exports = {
    entry: "./app.js",
    output: {
        path: __dirname,
        filename: "external.js",
        libraryTarget: "umd",
        library: "fincalc"
    },
    externals: [
    {
      'react': {
        root: 'React',
        commonjs2: 'react',
        commonjs: 'react',
        amd: 'react'
      },
      'react-dom': {
        root: 'ReactDOM',
        commonjs2: 'react-dom',
        commonjs: 'react-dom',
        amd: 'react-dom'
      }
    }
  ],

    module: {
        loaders: [
            {
              test: /\.js$/,
              exclude: /node_modules/,
              loader: 'babel'
            }
        ]
    }
};
